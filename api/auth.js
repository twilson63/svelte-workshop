const jwt = require('jsonwebtoken')
const secret = process.env.SECRET || 'mysecret'

module.exports = (req, res, next) => {
  const token = jwt.sign({username: 'twilson63'}, secret)
  res.writeHead(200, { 'Content-Type': 'application/json'})
  res.end(JSON.stringify({
    token
  }))

}
