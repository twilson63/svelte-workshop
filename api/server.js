const jsonServer = require('json-server')
const multer = require('multer')

const server = jsonServer.create()
const router = jsonServer.router('./db.json')

const auth = require('./auth.js')
const verify = require('./verify.js')

const upload = multer({ dest: './uploads'})

server.use(jsonServer.defaults())
server.use('/auth', auth)
server.use('/api', verify)
server.use('/api', router)

server.use('/files', verify, upload.single('media'), (req, res) => {
  // TODO: post file to mux.com api for playbackId

  res.writeHead(201, {'Content-Type': 'application/json'})
  res.end(JSON.stringify({
    mediaId: req.file.filename
  }))
})

server.listen(3000, () => {
  console.log('api running on 3000')
})


