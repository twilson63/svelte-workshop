const jwt = require('jsonwebtoken')

const secret = process.env.SECRET || 'mysecret'

// secure endpoints
module.exports = (req, res, next) => {
  if (!req.headers.authorization) {
    res.writeHead(401, { 'Content-Type': 'application/json'})
    res.end(JSON.stringify({error: 'Not Authorized'}))
    return
  }
  const token = req.headers.authorization.split(' ')[1]
  console.log(token)
  jwt.verify(token, secret, (err, decoded) => {
    console.log(err)

    if (err) {
      res.writeHead(401, { 'Content-Type': 'application/json'})
      res.end(JSON.stringify({error: 'Invalid Token'}))
      return
    }
    req.token = decoded
    next()
  })
}
