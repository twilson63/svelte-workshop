---
title: Svelte Workshop
description: this is a 90 minute workship focused from a quick intro to svelte to a fully working crud application using svelte to give attendees a comprehensive view of the svelte framework.
---
# Svelte Workshop

A 90 minute deep dive into sveltejs starting with quick introduction of the sveltejs framework basics with a comprehensive hands on implementation of a video admin portal example application. The video portal will allow administrators of a video screencast site to add new videos with meta data to present for their screencast customers. The admin portal will allow administrators to login, view a list of videos, add new videos, edit video data and remove video records. This custom content management site will demonstrate a large feature set of the svelte component framework.


Event: OpenSource 101 - https://opensource101.com/events/columbia-2020/     
Location: Columbia, SC        
Date: March 3, 2020       
Presenter: Tom Wilson, CTIO, Tabula Rasa HealthCare

## Agenda

(15 Minutes)

* Intro to Svelte
* Setup development environment

(45 Minutes)

### Basics

* Create page placeholders
* Configure router
* Setup Store Object
* Create Video Record
* List Video Records
* Show Video Record
* Edit Video Record
* Delete Video Record

(30 Minutes)

### Advanced

* Add animation/transitions
* Add authentication
* Add pagination
* Add searching

## Pre-Reqs

In order to get the most of this workshop, you want to be familiar with the following:

* HTML
* JavaScript
* CSS
* Git
* NodeJS/NPM
* Component based architecture

## Tools

You will need the following to participate in this workshop

* NodeJS - https://nodejs.org
* VSCode - https://code.visualstudio.com (or equivalent text editor)
* Git - https://git-scm.com
* Yarn - https://yarnpkg.com

### Intro to Svelte

In this section, we will spend 15 minutes getting up to speed on svelte basics:

* Svelte Component Structure
* Reactive declarations
* Template Directives
* Props, Events, Bindings
* Lifecycle, Stores
* Composition, Context, and Special Elements
* Debugging

### Setup your development environment

``` sh
npx degit sveltejs/template video-admin
cd video-admin
yarn
```

> Setup to run as single page application

Edit package.json start script to run in the single page application mode.

./package.json

``` json
{ 
  ...
  "scripts": {
      ...
      "start": "sirv public -s"
   }
}
```

> Remove padding from global css

In the global css page, we want to remove the padding of 8px so that we have full control of the viewport.

./public/global.css

``` css
body {
  ...
  padding: 0;
}
```

#### Setup Demo Backend Server

In order to build our application we need a backend server that supports 
authentication, the ability to upload files and the ability to manage
video objects. We will use a simple nodejs server that has the following
end points:

Endpoints

* /auth - authorizes user and returns a valid jwt
* /api/videos - creates a new video object and lists videos
* /api/videos/:id - gets single video, updates, and removes a video object
* /files - uploads a new media file

#### Run the server

create a new terminal/console window and follow the instructions

```
cd svelte-workshop/api
yarn
yarn start
```

if the server is running correctly you should see api running on port 3000

### Create svelte pages

### Create routes

### Create Store Object

### Create Video Record
### List Video Records
### Show Video Record
### Edit Video Record
### Delete Video Record


### Add animation/transitions
### Add authentication
### Add pagination
### Add searching


